#!/bin/sh

if [ -r /etc/docker-entrypoint-init.d/10-env.sh ]; then
    . /etc/docker-entrypoint-init.d/10-env.sh
fi
